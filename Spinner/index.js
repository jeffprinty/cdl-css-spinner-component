import React, { PropTypes } from 'react';
import styled, { keyframes } from 'styled-components';

const SpinnerContainer = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center; 

  width: 100%;
  height: 100%;
  background: rgba(255, 255, 255, 0.9);
  z-index: 1000;
`;

const SpinnerWrap = styled.div`
  margin: 100px auto;
  width: 50px;
  height: 20px;
  text-align: center;
  font-size: 10px;
  > div {
    background-color: ${props => props.color};
  }
`;
const oscillate = keyframes`
  0%, 40%, 100% { 
    transform: scaleY(0.35);
  }  20% { 
    transform: scaleY(1.9);
  }
`;
const Rect = styled.div`
  height: 100%;
  width: 6px;
  margin: 1px;
  display: inline-block;
  
  animation: ${oscillate} 1.04s infinite ease-in-out;
  animation-delay: ${props => props.delay}s;
`;

const Spinner = ({color='#DA1B2C'}) => {
  return (
    <SpinnerContainer>
      <SpinnerWrap color={color}>
        <Rect delay={-0.9} />
        <Rect delay={-0.8} />
        <Rect delay={-0.7} />
        <Rect delay={-0.6} />
        <Rect delay={-0.5} />
      </SpinnerWrap>
    </SpinnerContainer>
  );
};

export default Spinner;
