# ML CDL CSS Spinner Component

Implementation of the CDL Spinner gif as a component with CSS animations.
https://macmillanlearning.atlassian.net/wiki/pages/viewpage.action?pageId=69402691

```
import Spinner from './Spinner';
...
// Default to Macmillan red but accepts a hex color
<Spinner color="#DA1B2C" />
```